#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <algorithm> 
#include <fstream>
#include <sstream>
#include "Student.h"
using namespace std;

class StudentManagmentSystem {
	
	
public:

	vector<Student> students;
	static bool alphaComp(Student &a, Student &b);
	static bool gradeComp(Student &a, Student &b);
	StudentManagmentSystem();
	void addStudent();
	//show list of all students
	//void showStudentsOnCourse(string course);
	void showStudentsByName(string course);
	void showStudentsByAverageGrade(string course);
	//Search function
	Student showStudentById();
	//shows failing students
	void showFailingStudents();


	//add loading and saving students
	void saveStudents();
	void loadStudents();
	void parseLine(const string& s);
};
