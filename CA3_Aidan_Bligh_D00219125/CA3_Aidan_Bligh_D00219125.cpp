#include <string>
#include <iostream>
#include <vector>
#include "StudentManagmentSystem.h"
using namespace std;
//menu methods
void printMainMenu();
void printShowStudentMenu();
void printCourseOptions();
void printShowDataMenu();

//Student Managment System methods
void addStudent();
void showStudentsOnCourse();
void findStudentById();
void showFailingStudents();
void loadAndStoreData();

StudentManagmentSystem SMS;// = new StudentManagmentSystem();
int main()
{
	bool endMenu = false;
	while (!endMenu) 
	{
		int answerAsInt = -1;
		string answer = "";
		
		try 
		{
			//int choice = 0;
			printMainMenu();
			getline(cin, answer);
			answerAsInt = stoi(answer);
			//cin >> choice;
			switch (answerAsInt)
			{
				//find out if you can use enums
			case 1:
				addStudent();

				break;
			case 2:
				showStudentsOnCourse();
				break;
			case 3:
				findStudentById();
				break;
			case 4:
				showFailingStudents();
				break;
			case 5:
				loadAndStoreData();
				break;
			case 6:
				cout << "Enter 1 if you want to quit.(You may want to save your changes)" << endl;
				string leave;
				cin >> leave;
				if (leave == "1") { endMenu = true; }
			}
		}
		catch (invalid_argument e) 
		{
			cout << "something broke with the input, going back to main" << endl;
			main();
			endMenu = true;
		}
		
	}
	cout << "Thank you for using my student management system";
}

void printMainMenu() 
{
	cout << "Enter 1 to add student.\n";
	cout << "Enter 2 to show students on a course.\n";
	cout << "Enter 3 to find and print student by student Id. (Doesn't work due to some weird invalid argument exception I already have a catch for. Id's dont increment anyway.)\n";
	cout << "Enter 4 to show failing students.\n";
	cout << "Enter 5 to enter the data menu.\n";
	cout << "Enter 6 to exit.\n";
}
void printShowStudentMenu() 
{
	cout << "Enter 1 to print students in order of student name.\n";
	cout << "Enter 2 to print students in order of average grades.\n";
	cout << "Enter 3 to exit.\n";
}

void printCourseOptions()
{
	cout << "Enter 1 to show students in Computing" << endl;
	cout << "Enter 2 to show students in Web Development" << endl;
	cout << "Enter 3 to show students in Game Development" << endl;
	
}

void printShowDataMenu()
{
	cout << "Enter 1 to Save the students currently in the system to students.txt (Will overide whatevers currently in students.txt)" << endl;
	cout << "Enter 2 to Load the students currently stored in students.txt" << endl;
	cout << "Enter 3 quit" << endl;
	
}

void addStudent()
{
	SMS.addStudent();
	
}

void showStudentsOnCourse()
{
	cin.clear();
	string course;
	int answerAsInt = -1;
	string answer = "";
	try
	{	
		printCourseOptions();
		getline(cin, answer);
		answerAsInt = stoi(answer);
		while (answerAsInt < 1 || answerAsInt>4) 
		{
			cout << "Please enter an answer between 1 and 4" << endl;
			printCourseOptions();
			getline(cin, answer);
			answerAsInt = stoi(answer);
			cin.sync();
		}
		switch (answerAsInt)
		{
		case 1: 
			course = "Computing";
			break;
		case 2:
			course = "Web Development";
			break;
		case 3:
			course = "Game Development";
			break;
		case 4:
			course = "Data nalytics";
		}
		answerAsInt = -1;
		answer = "";
		while (answerAsInt < 1 || answerAsInt>3)
		{
			cout << "Please enter an answer between 1 and 3" << endl;
			printShowStudentMenu();
			getline(cin, answer);
			answerAsInt = stoi(answer);
			cin.sync();
		}
		switch (answerAsInt)
		{
		case 1:
			SMS.showStudentsByName(course);
			break;
		case 2:
			SMS.showStudentsByAverageGrade(course);
			break;
		case 3:
			cout << "Returning to menu" << endl;
		}
	}
	catch (invalid_argument& e)
	{
		cout << "Something messed up with the buffer, Try not to input letters or spaces. Try again" << endl;
		showStudentsOnCourse();
	}

}

void findStudentById()
{
	SMS.showStudentById();
}

void showFailingStudents()
{
	SMS.showFailingStudents();
}

void loadAndStoreData()
{
	int answerAsInt = -1;
	string answer = "";
	try
	{
		answerAsInt = -1;
		answer = "";
		while (answerAsInt < 1 || answerAsInt>3)
		{
			cout << "Please enter an answer between 1 and 3" << endl;
			printShowDataMenu();
			getline(cin, answer);
			answerAsInt = stoi(answer);
			cin.sync();
		}
		switch (answerAsInt)
		{
		case 1:
			SMS.saveStudents();
			break;
		case 2:
			SMS.loadStudents();
			break;
		case 3:
			cout << "Returning to menu" << endl;
			break;
		}
	}
	catch (invalid_argument& e)
	{
		cout << "Something messed up with the buffer, Try not to input letters or spaces. Try again" << endl;
		loadAndStoreData();
	}
}

