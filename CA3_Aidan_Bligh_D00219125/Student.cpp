#include "Student.h"


int Student::findAverageGrade()
{
	return (this->scores[0] + this->scores[1] +this->scores[2] + this->scores[3]) /4;
}

Student::Student()
{
	this->studentId = -1;
	this->title = chooseTitle();
	this->name = addName();
	this->course = chooseCourse();
	this->scores = addScores();
	this->averageGrade = findAverageGrade();
	cout << "Student Added." << endl;
}
Student::Student(int id, string title, string name, int* scores, string course) 
{
	this->studentId = id;
	this->title = title;
	this->name = name;
	this->course = course;
	this->scores = scores;
}

string Student::addName()
{
	string firstName = "";
	string lastName = "";
	//string::size returns value in bytes
	int MIN_SIZE = 4;
	cout << "Enter the students first name." << endl;
	getline(cin, firstName);
	while (firstName.size() < MIN_SIZE)
	{
		cout << "please have a longer name" << endl;
		cout << "enter a longer name." << endl;
		getline(cin, firstName);
	}
	cout << "Enter the students last name." << endl;
	getline(cin, lastName);
	while (lastName.size() < MIN_SIZE)
	{
		cout << "please have a longer name" << endl;
		cout << "enter a longer name." << endl;
		getline(cin, lastName);
	}
	return firstName + " " + lastName;
}

string Student::chooseTitle()
{
	cin.sync();
	string choice = "";
	while (1) 
	{
		cout << "Enter the students title.(Mr, Ms or Mrs)" << endl;
		getline(cin, choice);
		if (choice.compare("Mr") == 0 || choice.compare("mr") == 0 || choice.compare("MR") == 0) { return "Mr";}
		else if(choice.compare("Mrs") == 0 || choice.compare("mrs") == 0 || choice.compare("MRS") == 0){return "Mrs";}
		else if( choice.compare("Ms") == 0 || choice.compare("ms") == 0 || choice.compare("MS") == 0){return "Ms";}
	}
}

string Student::chooseCourse()
{
	cin.sync();
	cout << "Hello World!\n";
	string course = "unassigned";
	bool confirmed = false;
	int choice;
	while (!confirmed)
	{
		cout << "Enter the course number of the student\n1)Computing\n2)Web Development\n3)Game Development\n4)Data Science"<<endl;
		choice = 0;
		cin >> choice;
		switch (choice)
		{
		case 1:
			course = "Computing";
			cin.sync();
			confirmed = true;//confirmCourseChoice(course);
			break;
		case 2:
			course = "Web Development";
			cin.sync();
			confirmed = true;// confirmCourseChoice(course);
			break;
		case 3:
			course = "Game Development";
			cin.sync();
			confirmed = true;// confirmCourseChoice(course);
			break;
		case 4:
			course = "Data Science";
			cin.sync();
			confirmed = true;// confirmCourseChoice(course);
			break;
		}
	}
	cout << "You have confirmed that the student is doing " + course +"."<<endl;
	return course;
}

int* Student::addScores()
{
	cin.sync();
	cout << "What are " + name + "'s scores for " +course + "?";
	
	int* score = new int [4];
	try
	{
	for (int i = 0; i < 4; i++) 
	{
		int answerAsInt = -1;
		string answer = "";
		cout << "---------" << endl;
		cout << "Enter score " + i << endl;
		getline(cin, answer);
		answerAsInt = stoi(answer);
		while (answerAsInt < 0 || answerAsInt > 100)
		{
			cin.sync();
			cout << "Please enter a score between 0 and 100" << endl;
			getline(cin, answer);
			answerAsInt = stoi(answer);
		}
		score[i] = answerAsInt;
		cout << "You entered "<<answerAsInt<<endl;
	}
	}
	catch(invalid_argument &e)
	{
		cout << "Something messed up with the buffer, Try not to input letters or spaces" << endl;
		score = addScores();
	}
	return score;
}

string Student::getTitle()
{
	return this->title;
}

string Student::getName()
{
	return this->name;
}

string Student::getCourse()
{
	return this->course;
}

int Student::getId()
{
	return this->studentId;
}

int* Student::getScores()
{
	return this->scores;
}

int Student::getAveGrade()
{
	return this->averageGrade;
}

void Student::setTitle(string title)
{
	this->title = title;
}

void Student::setName(string name)
{
	this->name = name;
}

void Student::setCourse(string course)
{
	this->course = course;
}

void Student::setId(int id)
{
	this->studentId = id;
}

void Student::setScores(int* scores)
{
	this->scores = scores;
}

bool Student::operator==(Student s)
{
	if(this->studentId == s.studentId) 
	{
		cout << "Warning Students" << this->name << " and " <<s.name <<"share ID's" << endl;
		if (this->name.compare(s.name) == 0) 
		{
			cout << "Warning Students" << this->name << " and " << s.name << "share ID's and names" << endl;
			if (this->course.compare(s.course) == 0) 
			{
				cout << "Warning Students" << this->name << " and " << s.name << "share ID's, names and courses. They are likely dupelicates" << endl;
				return true;
			}
		}
	}
	return false;
}

bool Student::operator!=(Student s)
{
	if(this->studentId == s.studentId)
	{
		cout << "Warning Students" << this->name << " and " << s.name << "share ID's" << endl;
		return false;
	}
	return true;
}

Student& Student::operator=(const Student& s)
{
	this->title = s.title;
	this->name = s.name;
	this->course = s.course;
	this->studentId = s.studentId;
	this->scores = new int[4];
	for (int i = 0; i < 4; i++) 
	{
		this->scores[i] = s.scores[i];
	}
	return *this;
}

ostream& operator<<(ostream& strm, const Student& stu)
{
	int score1 = stu.scores[0], score2 = stu.scores[1], score3 = stu.scores[2], score4 = stu.scores[3];
	return strm << "Title: " << stu.title << "\nName: " << stu.name << "\nId: " << stu.studentId << "\nCourse: " << stu.course << //testArr[0] << testArr[1]<<
		"\nScores: " << score1 <<", " <<score2 << ", "<< score3<< ", " << score4 << endl;
}
//--------------unused/scraped methods-----------------------
/*
istream& operator>>(istream& input, const Student& stu)
{
	input >> stu.studentId >> ";" >> students[i].getTitle() << ";" << students[i].getName() << ";" << students[i].getScores() << ";" << students[i].getCourse();
}*/
/*
bool Student::confirmCourseChoice(string choice)
{
	cin.sync();
	string answer = "";
	cout << "Are you sure the student is doing " + choice + "? Y/N" << endl;
	getline(cin, answer);
	while (answer.compare("y") == 0 || answer.compare("Y") == 0 || answer.compare("YES") == 0 || answer.compare("yes") == 0
		|| answer.compare("n") == 0 || answer.compare("N") == 0 || answer.compare("No") == 0 || answer.compare("no") == 0)
	{
		cout << "Please enter a valid choice. (Y or N)" << endl;
		getline(cin, answer);
	}
	if (answer == "y" || answer == "Y" || answer == "Yes" || answer == "yes") { return true; }
	if (answer == "n" || answer == "N" || answer == "No" || answer == "no") { return false; }
}
*/
