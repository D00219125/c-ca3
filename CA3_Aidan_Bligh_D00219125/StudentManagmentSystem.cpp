#include "StudentManagmentSystem.h"



bool StudentManagmentSystem::alphaComp(Student& a, Student& b)
{
	return a.getName()<b.getName();
}

bool StudentManagmentSystem::gradeComp(Student& a, Student& b)
{
	return a.getAveGrade()<b.getAveGrade();
}


StudentManagmentSystem::StudentManagmentSystem()
{
	cout << "Student Managment Ststem Activated" << endl;
	
}

void StudentManagmentSystem::addStudent()
{
	
	Student stu = Student();
	stu.setId(stu.getId()+1);
	
	cout << stu;
	//find out if you shold be putting &stu in instead of stu
	bool copy = false;
	for (int i = 0; i < students.size(); i++)
	{
		if (stu == students[i])
		{
			copy = true;
		}
	}
	if (!copy)
	{
		students.push_back(stu);
	}
	else
	{
		cout << "There is a copy of a student currently in the system in students.txt. Student " << stu.getName() << " id: " << stu.getId() << "will not be added" << endl;
	}
}

void StudentManagmentSystem::showStudentsByName(string course)
{
	int size = students.size();
	sort(students.begin(), students.end(), alphaComp);
	for (int i = 0; i < size;i++) 
	{
		if (students[i].getCourse().compare(course) == 0) 
		{
			cout << "----------" << students[i] << "----------" << endl;
		}
		
	}
}

void StudentManagmentSystem::showStudentsByAverageGrade(string course)
{
	if (students.empty()) 
	{
		cout << "You do not have any students at the moment. Try, adding some or loading them in from a save." << endl;
	}
	else 
	{
		int size = students.size();
		sort(students.begin(), students.end(), gradeComp);
		for (int i = 0; i < size; i++)
		{
			if (students[i].getCourse().compare(course) == 0)
			{
				cout << "----------" << students[i] << students[i].getAveGrade() << "----------" << endl;
			}
		}
	}
	
}

Student StudentManagmentSystem::showStudentById()
{
	cin.sync();
	if (!students.empty()) 
	{
		try 
		{
			int ans = -1;
			
			while (ans < 0) 
			{
				cout << "Enter the students id" << endl;
				string s;
				getline(cin, s);
				ans = stoi(s);
			}
			int pos = 0;
			for (int i = 0; i < students.size(); i++) 
			{
				if (students[i].getId() == ans) { pos = i; }
			}
			Student stuCopy = students[pos];
			cout << stuCopy;
			return stuCopy;
		}
		catch (invalid_argument& e) 
		{
			cout << "don't enter letters please, that breaks stuff." << endl;
			showStudentById();
		}
		//catch(read access violation cant be caught)
	}
	else 
	{
		cout << "You do not have any students at the moment. Try, adding some or loading them in from a save." << endl;
		
		int var =-1;
		int* ptr = &var;
		return Student(var,"unassinged","unassigned",ptr,"unassigned");
	}
}

void StudentManagmentSystem::showFailingStudents()
{
	if (students.empty()) 
	{
		cout << "You do not have any students at the moment. Try, adding some or loading them in from a save." << endl;
	}
	else 
	{
		for (int i = 0; i < students.size(); i++)
		{
			if (students[i].getAveGrade() < 40)
			{
				cout << "------------" << endl;
				cout << students[i] << endl;
				cout << "------------" << endl;
			}
		}
	}
}

void StudentManagmentSystem::saveStudents()
{
	cout << "Saving students to students.txt" << endl;
	ofstream outstream("students.txt");
	if (outstream.good()) 
	{
		for (int i = 0; i < students.size(); i++) 
		{
			Student s = students[i];
			outstream << students[i].getId() << ";" << students[i].getTitle() << ";" << students[i].getName() << ";";
			//this will fix
			
			for (int j = 0; j < 4; j++) 
			{
				outstream << students[i].scores[j] << ",";
			}
			outstream << ";" << students[i].getCourse() <<";";
			outstream.close();
		}
	}
	else 
	{
		cout << "file wont open." <<endl;
	}
}

void StudentManagmentSystem::loadStudents()
{
	cout << "Reading from semicolon-delimited text file: students.txt" << endl;
	string line;
	ifstream inStream("students.txt");
	if (inStream.good()) 
	{
		while (getline(inStream, line)) 
		{
			parseLine(line);
		}
		inStream.close();
	}
	else 
	{
		cout << "cant find file or it's empty" << endl;
	}

}

void StudentManagmentSystem::parseLine(const string& s)
{
	bool copy = false;
	stringstream strstrm(s);
	string title, name, course;
	int id;
	int* scores = new int[4];
	try
	{
		string str;
		getline(strstrm, str, ';');
		id = stoi(str);
		getline(strstrm, str, ';');
		title = str;
		getline(strstrm, str, ';');
		name = str;
		for (int i = 0; i < 4; i++) 
		{
			getline(strstrm, str, ',');
			scores[i] = stoi(str);
		}
		getline(strstrm, str, ';');
		getline(strstrm, str, ';');
		course = str;
		Student s = Student(id, title, name, scores, course);
			students.push_back(s);
			cout << "student added";
			cout << s;
	}
	catch (invalid_argument const& e) 
	{

	}
}

