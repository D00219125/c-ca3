#pragma once
#include <string>
#include <iostream>

using namespace std;

class Student 
{
	string title = "unassigned", course = "unassigned";
	string  name = "unassigned";
	int averageGrade;
	int studentId ;
public:
	
	//these have to be public for the student managment system
	int findAverageGrade();
	int* scores;
	//Student Methods
	Student();
	Student(int id, string title, string name, int* scores, string course);
	string addName();
	string chooseTitle();
	string chooseCourse();
	int* addScores();

	//getters
	string getTitle();
	string getName();
	string getCourse();
	int getId();
	int* getScores();
	int getAveGrade();
	//setters
	void setTitle(string title);
	void setName(string name);
	void setCourse(string course);
	void setId(int id);
	void setScores(int* scores);
	//toString Essentially
	friend ostream& operator<<(ostream& strm, const Student& stu);
	//== will be used to check for duplicates
	bool operator==(Student s);
	bool operator!=(Student s);
	//assignment operator makes s = the current student
	Student& operator=(const Student& s);
	//for bitshifting which I didn't do because I forgot that part of the breif until now
	friend istream& operator>>(istream& input, const Student& stu);

};